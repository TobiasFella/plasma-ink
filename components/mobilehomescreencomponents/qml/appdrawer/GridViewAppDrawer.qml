/*
 *  SPDX-FileCopyrightText: 2021 Marco Martin <mart@kde.org>
 *  SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.15 as Controls

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PC3
import org.kde.plasma.extras 2.0 as PlasmaExtra
import org.kde.kirigami 2.10 as Kirigami

import org.kde.plasma.private.nanoshell 2.0 as NanoShell
import org.kde.plasma.private.inkshell 1.0 as MobileShell

import org.kde.plasma.private.mobilehomescreencomponents 0.1 as HomeScreenComponents

import "../private"
import "../"

Item {
    id: root

    property real leftPadding: 0
    property real topPadding: 0
    property real bottomPadding: 100
    property real rightPadding: 0

    signal launched

    readonly property int reservedSpaceForLabel: metrics.height

    PC3.Label {
        id: metrics
        text: "M\nM"
        visible: false
        font.pointSize: PlasmaCore.Theme.defaultFont.pointSize * 0.9
    }

    ColumnLayout {
        anchors.fill: parent
        MobileShell.BaseItem {
            Layout.fillWidth: true
            Layout.fillHeight: true
            leftPadding: root.leftPadding;
            topPadding: root.topPadding
            rightPadding: root.rightPaDding;

            contentItem: GridView {
                id: gridView
                clip: true
                interactive: false

                cellWidth: root.width / Math.floor(root.width / (cellHeight - root.reservedSpaceForLabel + PlasmaCore.Units.smallSpacing*4))
                cellHeight: PlasmaCore.Units.iconSizes.large + reservedSpaceForLabel

                property int columns: Math.floor(root.width / cellWidth)
                property int rows: Math.ceil(model.count / columns)

                cacheBuffer: Math.max(0, rows * cellHeight)

                model: HomeScreenComponents.ApplicationListModel

                Component.onCompleted: {
                    HomeScreenComponents.ApplicationListModel.perPage = columns * Math.floor(root.height / cellHeight)
                }

                delegate: DrawerGridDelegate {
                    id: delegate

                    width: gridView.cellWidth
                    height: gridView.cellHeight
                    reservedSpaceForLabel: root.reservedSpaceForLabel

                    onLaunch: (x, y, icon, title, storageId) => {
                        if (icon !== "") {
                            MobileShell.HomeScreenControls.openAppAnimation(
                                icon,
                                title,
                                delegate.iconItem.Kirigami.ScenePosition.x + delegate.iconItem.width/2,
                                delegate.iconItem.Kirigami.ScenePosition.y + delegate.iconItem.height/2,
                                Math.min(delegate.iconItem.width, delegate.iconItem.height));
                        }

                        HomeScreenComponents.ApplicationListModel.setMinimizedDelegate(index, delegate);
                        HomeScreenComponents.ApplicationListModel.runApplication(storageId);
                        root.launched();
                    }
                }
            }
        }
        RowLayout {
            Layout.preferredHeight: 30
            PC3.Button {
                Layout.fillWidth: true
                Layout.preferredHeight: 30
                onClicked: HomeScreenComponents.ApplicationListModel.offset -= HomeScreenComponents.ApplicationListModel.perPage
                icon.name: "arrow-left"
            }
            PC3.Button {
                Layout.fillWidth: true
                Layout.preferredHeight: 30
                onClicked: HomeScreenComponents.ApplicationListModel.offset += HomeScreenComponents.ApplicationListModel.perPage
                icon.name: "arrow-right"
            }
        }
        Item {
            Layout.preferredHeight: root.bottomPadding
        }
    }
}

