/*
 *   SPDX-FileCopyrightText: 2014 Marco Martin <notmart@gmail.com>
 *   SPDX-FileCopyrightText: 2021 Devin Lin <devin@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.plasma.private.nanoshell 2.0 as NanoShell

import "../components" as Components

NanoShell.FullScreenOverlay {
    id: window

    property real offset: contentContainerLoader.maximizedQuickSettingsOffset

    property int mode: (height > width && width <= largePortraitThreshold) ? ActionDrawer.Portrait : ActionDrawer.Landscape
    
    /**
     * At some point, even if the screen is technically portrait, if we have a ton of width it'd be best to just show the landscape mode.
     */
    readonly property real largePortraitThreshold: PlasmaCore.Units.gridUnit * 35
    
    enum Mode {
        Portrait = 0,
        Landscape
    }
    
    width: Screen.width
    height: Screen.height
    
    color: "transparent"

    function close() {
        visible = false
    }
    function open() {
        visible = true
    }

    Loader {
        id: contentContainerLoader

        property real minimizedQuickSettingsOffset: item ? item.minimizedQuickSettingsOffset : 0
        property real maximizedQuickSettingsOffset: item ? item.maximizedQuickSettingsOffset : 0

        y: 0
        width: window.width
        height: window.height

        sourceComponent: window.mode == ActionDrawer.Portrait ? portraitContentContainer : landscapeContentContainer
    }

    Component {
        id: portraitContentContainer
        PortraitContentContainer {
            actionDrawer: window
            width: window.width
            height: window.height
        }
    }

    Component {
        id: landscapeContentContainer
        LandscapeContentContainer {
            actionDrawer: window
            width: window.width
            height: window.height
        }
    }
}
