/*
 *   SPDX-FileCopyrightText: 2015 Marco Martin <mart@kde.org>
 *
 *   SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include <Plasma/Containment>

class InkPanel : public Plasma::Containment
{
    Q_OBJECT

public:
    InkPanel(QObject *parent, const QVariantList &args);
};
